# README #

project structure:
- asstes
  - css
     - bootstrap : folder contains bootstrap files
     - main.css  : file contains all styles for layout
     - media.css : file contains all media queries for the project
  - fonts :folder contain fonts
  - img   : folder contain pictures
  - js    : folder contain javascripts
    - main.js : file contain logic of the page
    - paper-bootstrap-wizard.js : file contain logic of multistep form
- index.html - file , html layout

### Main information ###

In index.html we have two main blocks with: id="wizard-card" & id="login-card"
this is two card layouts ,wizard-card - is multistep wizard& login-card - is main login card.By default we hide wizard-card and show login card using javascript in main.js. In file paper-bootstrap-wizard.js we have logic of wizard multistep form.
### How do I get set up? ###

### css & layout ###

All main css rules of the page are in main.css. I left comments in most needed  parts of the file which describes the functionality.
