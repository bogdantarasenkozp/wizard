$(document).ready(function(){
  $('.hours-btn').click(function(){
    $('.check').removeClass('active');
    var btn = $(this);
    btn.children('.check').addClass('active');
  })
  $('#email-btn').click(function(){
    $("#login-card").fadeOut(function(){
      $("#wizard-card").fadeIn();
    });
  })
  $('.back-to-login').click(function(e){
    $("#wizard-card").fadeOut(function(){
      $("#login-card").fadeIn();
    });
  })
})
